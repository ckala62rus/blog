<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUpload extends Model
{
    public $image;

    /*
     * Что бы ни кто не мог загрузить что либо, кроме картинки, делаем валидацию
     * */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions'=>'jpg,png'],
        ];
    }

    /*Получаем файл из контроллера SetImage и записываем его в папку*/
    /*return возвращаем файл обратно в контроллер*/
    public function uploadFile(UploadedFile $file, $currentImage)
    {
        $this->image = $file;

        /*
         * Если отключена валидация JavaScript,
         * то сработает эта валидация PHP и выполнится код далее
         * */
        if ($this->validate())
        {
            //Если существует такая картинка, то удаляем её
            $this->deleteCurrentImage($currentImage);

            //Производим сохранение
            return $this->saveImage();
        }
    }

    /*
     * Путь к папке куда закачиваются картинки
     * */
    public function getFolder()
    {
        return Yii::getAlias('@web') . 'uploads/';
    }

    /*
     * Создает уникальное имя для картинки
     * */
    public function generateFileName()
    {
        return strtolower( md5 ( uniqid( $this->image->baseName ) ) ) . '.' . $this->image->extension;
    }

    /*
     * Удаляем картинку, если она есть
     * */
    public function deleteCurrentImage($currentImage)
    {
        /*
         * Проверка на существование файла и null
         * иначе выдает ошибку PHP permission dinided
         * */
        if ( !empty ($currentImage) && $currentImage != null)
        {
            if ( file_exists( $this->getFolder() . $currentImage ) )
            {
                unlink( $this->getFolder() . $currentImage);
            }
        }
    }

    /*
     * Производит сохранение файла
     * */
    public function saveImage()
    {
        $filename = $this->generateFileName();

        $this->image->saveAs($this->getFolder() . $filename);

        return $filename; //возвращает название картинки
    }
}