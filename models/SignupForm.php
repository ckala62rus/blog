<?php

namespace app\models;

use yii\base\Model;
use app\models\User;

class SignupForm extends Model
{
    public $name;
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['name', 'email', 'password'], 'required'],
            [['name'], 'string'],
            [['email'], 'email'],
            /*
             * 'targetClass'=>'app\models\User' с какой моделью будем связывать email.
             * 'targetAttribute'=>'email' с каким атрибутом будем связывать уникальность.
             * */
            [['email'], 'unique', 'targetClass'=>'app\models\User', 'targetAttribute'=>'email'],
        ];
    }

    /*
     * Регистрация нового пользователя
     * */
    public function signup()
    {
        if ($this->validate())
        {
            $this->password = md5($this->password);
            $user = new User();

            //В атрибуты модели User передаем атрибуты модели Signup
            $user->attributes = $this->attributes;

            return $user->create();
        }
    }

}