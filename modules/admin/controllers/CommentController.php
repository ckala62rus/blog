<?php

namespace app\modules\admin\controllers;

use app\models\Comment;
use yii\web\Controller;

class CommentController extends Controller
{
    /*
     * Вывод коминтариев с сортировкой desc по возрастанию
     * */
    public function actionIndex()
    {
        $comments = Comment::find()->orderBy('id desc')->all();

        return $this->render('index',[
            'comments'=>$comments,
        ]);
    }

    /*
     * Удаление коментария по $id
     * */
    public function actionDelete($id)
    {
        $comment = Comment::findOne($id);

        if ( $comment->delete() )
        {
            return $this->redirect(['comment/index']);
        }
    }

    /*
     * Разрешить комментарий
     * */
    public function actionAllow($id)
    {
        $comment = Comment::findOne($id);

        if ( $comment->allow() )
        {
            return $this->redirect(['index']);
        }
    }

    /*
     * Запретить коментарий
     * */
    public function actionDisallow($id)
    {
        $comment = Comment::findOne($id);

        if ( $comment->disallow() )
        {
            return $this->redirect(['index']);
        }
    }
}